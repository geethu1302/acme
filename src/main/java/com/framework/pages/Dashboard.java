package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.api.SeleniumBase;
import com.framework.design.ProjectMethods;

public class Dashboard extends ProjectMethods {
	Actions builder;
	public Dashboard() 
	   
	   {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
     }
	
	@FindBy(how = How.XPATH,using="//button[text()=' Vendors']") WebElement eleMouse;
	public Dashboard MOVendor(){
		 builder=new Actions(driver);		 
		 builder.moveToElement(driver.findElementByXPath("//button[text()=' Vendors']")).build().perform();
		return this;
	}	
	
	public SearchVendor ClickSearch()
	{
		builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();	   

		return new SearchVendor();
		
	}
	
	

}
