package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;



public class Login extends ProjectMethods {

	public Login() {
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how=How.ID,using="email") WebElement eleEmail;
	
	public Login enterEmail(String uname) {
		eleEmail.sendKeys(uname);
		return this;
		
		
	}
	@FindBy(how=How.ID,using="password") WebElement elePassword;
	public Login enterPassword(String pwd) {
		elePassword.sendKeys(pwd);
		
		return this;
		
	}
	@FindBy(how=How.ID,using="buttonLogin") WebElement eleLoginButton;
	public Dashboard clickLogin() {
		eleLoginButton.click();
		return new Dashboard();
		
	}
}
