package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchResult extends ProjectMethods{

	
	
	public SearchResult() 
	   
    {
	//apply PageFactory
	PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//td[text()='Moretronic Intelligence']") WebElement eleVerify;
	public SearchResult verifyText()
	{
		System.out.println(getElementText(eleVerify));
		return this;
	}

}
